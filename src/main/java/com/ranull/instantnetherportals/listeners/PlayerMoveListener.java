package com.ranull.instantnetherportals.listeners;

import com.ranull.instantnetherportals.InstantNetherPortals;
import com.ranull.instantnetherportals.nms.NMS;
import org.bukkit.GameMode;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEnterEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerMoveListener implements Listener {
    private InstantNetherPortals plugin;
    private NMS nms;

    public PlayerMoveListener(InstantNetherPortals plugin, NMS nms) {
        this.plugin = plugin;
        this.nms = nms;
    }

    @EventHandler
    public void onEntityPortalEnter(EntityPortalEnterEvent event)
    {
        if (!event.getEntityType().equals(EntityType.PLAYER) ||
                !event.getLocation().getBlock().getType().toString().equals("PORTAL")) {
            return;
        }

        Player player = (Player)event.getEntity();
        if (player.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }

        nms.setInvulnerable(player, true);

        new BukkitRunnable() {
            @Override
            public void run() {
                nms.setInvulnerable(player, false);
            }
        }.runTaskLater(plugin, 3);
    }
}
