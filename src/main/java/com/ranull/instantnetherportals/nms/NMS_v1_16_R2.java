package com.ranull.instantnetherportals.nms;

import net.minecraft.server.v1_16_R2.EntityPlayer;
import net.minecraft.server.v1_16_R2.PlayerAbilities;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;

public class NMS_v1_16_R2 implements NMS {
    public void setInvulnerable(Player player, boolean bool) {
        EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();

        try {
            PlayerAbilities playerAbilities = entityPlayer.abilities;
            Field field = playerAbilities.getClass().getDeclaredField("isInvulnerable");

            field.setAccessible(true);
            field.setBoolean(playerAbilities, bool);
            field.setAccessible(false);

            entityPlayer.updateAbilities();
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ignored) {
        }
    }
}
