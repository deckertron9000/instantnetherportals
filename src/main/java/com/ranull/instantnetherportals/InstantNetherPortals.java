package com.ranull.instantnetherportals;

import com.ranull.instantnetherportals.listeners.PlayerMoveListener;
import com.ranull.instantnetherportals.nms.*;
import org.bukkit.plugin.java.JavaPlugin;

public class InstantNetherPortals extends JavaPlugin {
    private NMS nms;

    @Override
    public void onEnable() {
        if (setupNMS()) {
            getServer().getPluginManager().registerEvents(new PlayerMoveListener(this, nms), this);
        } else {
            getLogger().severe("Version not supported, disabling plugin!");
            getServer().getPluginManager().disablePlugin(this);
        }
    }

    private boolean setupNMS() {
        try {
            String version = getServer().getClass().getPackage().getName().split("\\.")[3];

            if (version.equals("v1_7_R4")) {
                nms = new NMS_v1_7_R4();
            } else if (version.equals("v1_8_R3")) {
                nms = new NMS_v1_8_R3();
            } else if (version.equals("v1_9_R2")) {
                nms = new NMS_v1_9_R2();
            } else if (version.equals("v1_10_R1")) {
                nms = new NMS_v1_10_R1();
            } else if (version.equals("v1_11_R1")) {
                nms = new NMS_v1_11_R1();
            } else if (version.equals("v1_12_R1")) {
                nms = new NMS_v1_12_R1();
            } else if (version.equals("v1_13_R2")) {
                nms = new NMS_v1_13_R2();
            } else if (version.equals("v1_14_R1")) {
                nms = new NMS_v1_14_R1();
            } else if (version.equals("v1_15_R1")) {
                nms = new NMS_v1_15_R1();
            } else if (version.equals("v1_16_R1")) {
                nms = new NMS_v1_16_R1();
            } else if (version.equals("v1_16_R2")) {
                nms = new NMS_v1_16_R2();
            } else if (version.equals("v1_16_R3")) {
                nms = new NMS_v1_16_R3();
            }

            return nms != null;
        } catch (ArrayIndexOutOfBoundsException ignored) {
            return false;
        }
    }
}
